﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Experis_Task27.Models;
using Microsoft.EntityFrameworkCore;
namespace Experis_Task27.Controllers
   
{
    [Route("api/[controller]")]
    [ApiController]
    public class SupervisorController : ControllerBase
    {
        private readonly SupervisorContext _context;
        public SupervisorController(SupervisorContext context)
        {
            _context = context;
        }
        [HttpGet]
        public ActionResult<IEnumerable<Supervisor>> GetSupervisor()
        {
            //return new Supervisor{ Id=1,Name="Micheal Jackson",Level="Pro"};
            return _context.supervisors;
        }
        [HttpPost]
        public ActionResult<Supervisor> ConsumeSupervisor(Supervisor supervisor)
        {
            _context.supervisors.Add(supervisor);
            _context.SaveChanges();
            return CreatedAtAction("GetSpecificSupervisorByID", new Supervisor { Id = supervisor.Id }, supervisor);
        }
        [HttpGet("{id}")]
        public ActionResult<Supervisor> GetSpecificSupervisorByID(int id)
        {
            var supervisor = _context.supervisors.Find(id);
            return supervisor;
        }
        [HttpPut("{id}")]
        public ActionResult<Supervisor> UpdateSupervisor(int id,Supervisor supervisor)
        {
            if (id!=supervisor.Id)
            {
                return BadRequest();
            }
            _context.Entry(supervisor).State = EntityState.Modified;
            _context.SaveChanges();
            return NoContent();
        }
        [HttpDelete("{id}")]
        public ActionResult<Supervisor> DeleteSupervisor(int id)
        {
            var supervisor = _context.supervisors.Find(id);
            if (supervisor==null)
            {
                return NotFound();
            }
            _context.supervisors.Remove(supervisor);
            _context.SaveChanges();
            return supervisor;
        }
    }
}