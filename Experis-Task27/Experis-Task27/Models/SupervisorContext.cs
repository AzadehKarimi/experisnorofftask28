﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Experis_Task27.Models
{
    public class SupervisorContext:DbContext

    {
        public SupervisorContext(DbContextOptions<SupervisorContext> options):base(options)
        {

        }
        public DbSet<Supervisor> supervisors { get; set; }
    }
}
